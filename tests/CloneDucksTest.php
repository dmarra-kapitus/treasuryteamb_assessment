<?php


class CloneDucksTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Default Passing Test
     */
    public function testMallard()
    {
        $cloningMachine = new \Kapitus\CloningMachine();
        $mallard = $cloningMachine->run(new \Kapitus\Ducks\Mallard());
        static::assertTrue(method_exists($mallard, 'clone'));
        static::assertEquals('fly', $mallard->fly());
        static::assertEquals('swim', $mallard->swim());
        static::assertEquals('quack', $mallard->quack());
        static::assertNotTrue(method_exists($mallard, 'squeak'));
    }

    public function testCanvasback()
    {
        $cloningMachine = new \Kapitus\CloningMachine();
        $canvasback = $cloningMachine->run(new \Kapitus\Ducks\Canvasback());
        static::assertTrue(method_exists($canvasback, 'clone'));
        static::assertEquals('fly', $canvasback->fly());
        static::assertEquals('swim', $canvasback->swim());
        static::assertEquals('quonk', $canvasback->quack());
        static::assertNotTrue(method_exists($canvasback, 'squeak'));
    }

    public function testRubberDuck()
    {
        $cloningMachine = new \Kapitus\CloningMachine();
        $rubberDuck = $cloningMachine->run(new \Kapitus\Ducks\RubberDuck());
        static::assertTrue(method_exists($rubberDuck, 'clone'));
        static::assertNotTrue(method_exists($rubberDuck, 'fly'));
        static::assertNotTrue(method_exists($rubberDuck, 'swim'));
        static::assertEquals('squeak', $rubberDuck->squeak());
    }

    public function testGoose()
    {
        $cloningMachine = new \Kapitus\CloningMachine();
        $goose = $cloningMachine->run(new \Kapitus\Ducks\Goose());
        static::assertTrue(method_exists($goose, 'clone'));
        static::assertEquals('fly', $goose->fly());
        static::assertEquals('swim', $goose->swim());
        static::assertEquals('honk', $goose->quack());
        static::assertNotTrue(method_exists($goose, 'squeak'));
    }
}