<?php

namespace Kapitus;

use Kapitus\Ducks\DuckAbstract;

/**
 * Class CloningMachine
 * @package Kapitus
 */
class CloningMachine
{
    public function run(DuckAbstract $duck)
    {
        return $duck->clone();
    }
}