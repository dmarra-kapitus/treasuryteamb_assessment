<?php

namespace Kapitus\Ducks;

abstract class DuckAbstract
{
    public function clone()
    {
        return $this;
    }

    public function quack()
    {
        return 'quack';
    }

    public function swim()
    {
        return 'swim';
    }

    public function fly()
    {
        return 'fly';
    }
}