# Hello and thank you for interviewing at Kapitus.


###### Background

Welcome to Cloning Inc. Today is your first day replacing our old developer who left the company over _moral objections_.

A little bit about our business, we have an amazing machine that clones animals. However currently it only works on Mallards, sorta like "hotdog, not hotdog."

We had tried to run the machine on different animals but the results were not successful. Apparently the machine was really only built to clone Mallards, and we should have informed the developers that we were planning on producing other types of animals.

The machine was working perfectly when we had just Mallards however since we added different species the cloning machine has been producing flawed ducks. These creatures are duck-like but very much not what are customers are expecting us to produce.

This has been a major setback, which has cause several of our Quality Assurance personal to leave the company over _moral objections_. Luckly before resigning they produced some really solid unit testing to determine if the animal is correct or not. You can find these tests in the /test directory.

We want to thank you again for coming on board with such notice and hope that you can solve our cloning machines problems. We expect the PETA protesters to leave in the next few weeks.

###### Tasks

We added a few mock ups of the different animals we want to test in the src/Ducks directory. If you could please add the required code to get all of the tests to pass. Feel free to modify anything you would like. Given the sensitivity of this project "over engineering" is acceptable.

###### Goals

The goal of this project is to get all of the unit tests to pass.

###### Rules

You may modify the code anyway you want. The existing unit tests are a guide to show you where you want to go. You are not required to modify the /tests directory but if you choose so you are allowed.

When you have completed your task feel free to send a Pull Request in.

Good Luck!


